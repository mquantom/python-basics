
# Comprehensions

# List comprehension, single line functionality with list (returns a list)
mylist = [1, 4, 2, 7]
lc = [val*val for val in mylist]
print(lc)

mylist = [1, 4, 2, 7]
lc = [val*val for val in mylist if val%2 == 0]
print(lc)

# Dictionary Comprehension
mylist = [1, 2, 4, 3, 1, 6, 7, 6, 4, 2, 3, 5]
dc = {key:1 for key in mylist}
print(dc.keys())
