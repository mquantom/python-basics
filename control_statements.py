# Demonstrating control statements

# if control statement
a = "v"
b = "medbasdfsdfsfsfsf"
if a <= b:
    print(" Always True.")
    print(" All statements at this indent level will belong to this if block.")
else:
    print(" Always False")


a = 90
b = 7

if a <= b:
    print("a is <= b")
else:
    print("a is not <= b")

a = 90
b = 7

if a <= b:
    print("a is <= b")
elif b <= 10:
    print("b is less than 10")
else:
    print("a is not <= b")


a = 68

if a >= 20 and a <= 30:
    print(" a is in between 20 and 30")
else:
    print(" a is not in between 20 and 30")

a = 25
if 20 <= a <= 30:
    print(" a is in between 20 and 30")
else:
    print(" a is not in between 20 and 30")


a = 2

while a < 10:
    print(a)
    a += 1

# Ternary operator
a = 36
x = 200 if a < 56 else 300
print(f" The value of x is {x}")


a = 5
b = 3
print(f"{a*b}")
print(f"{a**b}")

a = "abc"
b = 3
print(f"{a*b}")

# For loops
for val in range(0, 10, 3):
    print(f"val is {val}")

astr = "quantom"
for st in astr:
    print(f"char is {st}")

mylist = [1, 3,4,5,6]
for val in mylist:
    print(f"list val is {val}")
else:
    print("End of the list.")

mydict = {"abc": 1, "def": 2}
for key in mydict:
    print(f"dict val is {key} : {mydict[key]}")
else:
    print("End of the list.")


mydict = {"abc": 1345, "def": 2657567}
if "quantom" in mydict:
    for key in mydict:
        print(f"dict val is {key} : {mydict[key]}")

mydict = {"abc": 1345, "def": 2657567, 'quantom': 456}
if "quantom" in mydict:
    for key in mydict:
        print(f"dict val is {key} : {mydict[key]}")

for key in mydict:
    if key == "quantom":
        break
    else:
        print(f"The key is {key}")

for val in range(20):
    if val % 2 == 0:
        continue
    print(f"The Val is {val}")

for val in range(20):
    if val % 2 == 0:
        pass
    else:
        print(f"The Val is {val}")
