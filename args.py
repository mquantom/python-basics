
# variable arguments
# * = variable arguments

def fun(*myargs):
    print(myargs)
    print(myargs[0])


fun("quantom", "devops", "training")


def fun(num, addnum, *myargs):
    print(myargs)
    print(num)
    print(myargs[0])


fun(2, 3, "quantom", "devops", "training")


# keyword variable arguments
def fun(**myargs):
    print(myargs)
    print(myargs['arg1'])
    print(myargs['arg3'])

fun(arg1="quantom", arg2="devops", arg3="training")
fun(a="1", b=2, c=5.0)
