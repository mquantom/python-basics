

# knowing the time since the epoch
from datetime import *
import time

epoch = time.time() # call time() function of time module
print(epoch / 60 * 60)

t = time.ctime() # ctime() with epoch time
print('Current date and time: ' + t)


now = datetime.now()    # Timestamp
print(now)
print('Date now: {}/{}/{}'.format(now.day, now.month, now.year))
print('Time now: {}:{}:{}'.format(now.hour, now.minute, now.second))
